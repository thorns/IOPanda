 /*@@
   @header    ioPandaGH.h
   @date      01 Oct 1999
   @author    Jonghyun Lee
   @desc
              The extensions to the GH structure from IOPanda.
   @enddesc
   @version   $Header$
 @@*/

#ifndef _IOPANDA_IOPANDAGH_H_
#define _IOPANDA_IOPANDAGH_H_ 1

#include "StoreNamedData.h"
#include "CactusBase/IOUtil/src/ioutil_Utils.h"


#ifdef __cplusplus
extern "C"
{
#endif

typedef struct
{
  /* default number of times to output */
  int out_every_default;

  /* number of times to output for each variable */
  CCTK_INT *out_every;

  /* the last iteration output for each variable */
  int *out_last;

  /* list of variables to output */
  char *out_vars;

  /* I/O request descriptions for all CCTK variables */
  ioRequest **requests;

  /* directory in which to output */
  char *out_dir;

  /* stop on I/O parameter parsing errors ? */
  int stop_on_parse_errors;
} pandaGH;


/* prototypes of functions to be registered as IOPanda's IO method */
int IOPanda_OutputGH (const cGH *GH);
int IOPanda_TriggerOutput (const cGH *GH, int);
int IOPanda_TimeFor (const cGH *GH, int);
int IOPanda_OutputVarAs (const cGH *GH, const char *var, const char *alias);

/* prototypes of other functions */
void IOPanda_CheckSteerableParameters (const cGH *GH);

#ifdef __cplusplus
} // extern "C"
#endif

#endif  /* _IOPANDA_IOPANDAGH_H_ */
