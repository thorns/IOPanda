 /*@@
   @file      Startup.c
   @date      01 Oct 1999
   @author    Jonghyun Lee
   @desc
              Startup routines for IOPanda.
   @enddesc
   @version   $Id$
 @@*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cctk.h"
#include "cctk_Flesh.h"
#include "cctk_GHExtensions.h"
#include "cctk_Parameters.h"
#include "CactusBase/IOUtil/src/ioGH.h"
#include "CactusBase/IOUtil/src/ioutil_Utils.h"
#include "ioPandaGH.h"
#include "Panda/c_interface.h"

/* the rcs ID and its dummy function to use it */
static const char *rcsid = "$Header$";
CCTK_FILEVERSION(CactusPUGHIO_IOPanda_Startup_c)


/********************************************************************
 ********************    External Routines   ************************
 ********************************************************************/
void IOPanda_Startup (void);
void IOPanda_Finalize (void);


/********************************************************************
 ********************    Internal Routines   ************************
 ********************************************************************/
static void *SetupGH (tFleshConfig *config, int convergence_level, cGH *GH);


 /*@@
   @routine   IOPanda_Startup
   @date      Fri May 21 1999
   @author    Thomas Radke
   @desc
              The startup registration routine for IOPanda.
              Registers the GH extensions needed for IOPanda
              along with its setup routine.
   @enddesc
   @calls     CCTK_RegisterGHExtension
              CCTK_RegisterGHExtensionSetupGH
@@*/
void IOPanda_Startup (void)
{
  /* check that thorn PUGH was activated */
  if (CCTK_GHExtensionHandle ("PUGH") >= 0)
  {
    CCTK_RegisterGHExtensionSetupGH (CCTK_RegisterGHExtension ("IOPanda"),
                                     SetupGH);
  }
  else
  {
    CCTK_WARN (1, "Thorn PUGH was not activated. "
                  "No IOPanda I/O methods will be enabled.");
  }
}


 /*@@
   @routine   IOPanda_Finalize
   @date      Fri May 21 1999
   @author    Thomas Radke
   @desc
              The termination routine for IOPanda.
              Shuts down the Panda subsystem.
   @enddesc
@@*/
void IOPanda_Finalize (void)
{
  Panda_Finalize ();
}


/********************************************************************
 ********************    Internal Routines   ************************
 ********************************************************************/
 /*@@
   @routine    SetupGH
   @date       Fri 01 Oct 1999
   @author     Jonghyun Lee
   @desc
               Allocates and sets up IOPanda's GH extension structure.
   @enddesc

   @calls      CCTK_RegisterIOMethod
               CCTK_RegisterIOMethodOutputGH
               CCTK_RegisterIOMethodOutputVarAs
               CCTK_RegisterIOMethodTimeToOutput
               CCTK_RegisterIOMethodTriggerOutput
               IOUtil_CreateDirectory

   @var        config
   @vdesc      flesh configuration structure (unused)
   @vtype      tFleshConfig *
   @vio        in
   @endvar
   @var        convergence_level
   @vdesc      convergence level (unused)
   @vtype      int
   @vio        in
   @endvar
   @var        GH
   @vdesc      pointer to grid hierarchy
   @vtype      cGH *
   @vio        in
   @endvar

   @returntype void *
   @returndesc
               pointer to the allocated GH extension structure
   @endreturndesc
@@*/
static void *SetupGH (tFleshConfig *config, int convergence_level, cGH *GH)
{
  int i, numvars;
  FILE *fp;
  pandaGH *myGH;
  const ioGH *ioUtilGH;
  const char *my_out_dir;
  DECLARE_CCTK_PARAMETERS


  /* suppress compiler warnings about unused variables */
  (void) (config + 0);
  (void) (convergence_level + 0);


  /* register IOPanda's routines as a new I/O method */
  i = CCTK_RegisterIOMethod ("IOPanda");
  CCTK_RegisterIOMethodOutputGH (i, IOPanda_OutputGH);
  CCTK_RegisterIOMethodOutputVarAs (i, IOPanda_OutputVarAs);
  CCTK_RegisterIOMethodTimeToOutput (i, IOPanda_TimeFor);
  CCTK_RegisterIOMethodTriggerOutput (i, IOPanda_TriggerOutput);

  /* allocate a new GH extension structure */
  myGH = malloc (sizeof (pandaGH));
  numvars = CCTK_NumVars ();
  myGH->requests = calloc (numvars, sizeof (ioRequest *));
  myGH->out_last = malloc (numvars * sizeof (int));

  myGH->out_vars = strdup ("");
  myGH->out_every_default = out_every - 1;

  for (i = 0; i < numvars; i++)
  {
    myGH->out_last[i] = -1;
  }

  myGH->stop_on_parse_errors = strict_io_parameter_check;
  if (! CCTK_Equals (verbose, "none"))
  {
    CCTK_INFO ("I/O Method 'IOPanda' registered: output of grid variables and "
               "hyperslabs thereof to Panda files");
  }
  IOPanda_CheckSteerableParameters (GH);

  /* get the name of IOPanda's output directory */
  my_out_dir = out_dir;
  if (*my_out_dir == 0)
  {
    my_out_dir = io_out_dir;
  }

  /* skip the directory pathname if output goes into current directory */
  if (strcmp (my_out_dir, "."))
  {
    i = strlen (my_out_dir);
    if (CCTK_Equals (out_mode, "onefile") || ! strstr (my_out_dir, "%u"))
    {
      myGH->out_dir = malloc (i + 2);
      strcpy (myGH->out_dir, my_out_dir);
      myGH->out_dir[i] = '/';
      myGH->out_dir[i+1] = 0;
    }
    else
    {
      myGH->out_dir = malloc (i + 20);
      sprintf (myGH->out_dir, my_out_dir, CCTK_MyProc (GH));
      strcat (myGH->out_dir, "/");
    }
  }
  else
  {
    myGH->out_dir = strdup ("");
  }

  /* create the output directory */
  ioUtilGH = CCTK_GHExtension (GH, "IO");
  i = IOUtil_CreateDirectory (GH, myGH->out_dir,
                              ! CCTK_Equals (out_mode, "onefile"),
                              ioUtilGH->ioproc);
  if (i < 0)
  {
    CCTK_VWarn (1, __LINE__, __FILE__, CCTK_THORNSTRING,
                "Problem creating IOPanda output directory '%s'",myGH->out_dir);
  }
  else if (i > 0 && CCTK_Equals (verbose, "full"))
  {
    CCTK_VInfo (CCTK_THORNSTRING,
                "IOPanda output directory '%s' already exists", myGH->out_dir);
  }

  if (CCTK_MyProc (GH) == 0)
  {
    fp = fopen ("FILEPREFIX", "w");
    fprintf (fp, "%s", myGH->out_dir);
    fclose (fp);
  }

  /* start the Panda subsystem */
  Panda_Create (ioUtilGH->ioproc_every, 1);

  return (myGH);
}
