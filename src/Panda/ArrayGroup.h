#ifndef Arraygroup_dot_h
#define Arraygroup_dot_h

class Array;
class ArrayLayout;
#include "List.h"
#include "definitions.h"

class ArrayGroup {
 protected:
  char             *name_;           /* Name of the arraygroup */
  int              num_of_arrays_;   /* Number of arrays in group */
  List             *list_;           /* List of arrays */
  int              io_strategy_;

  /* If all the arrays have the same io and compute node layouts */
  Boolean          common_layouts_;
  int              common_layout_rank_;
  ArrayLayout      *compute_layout_;
  Distribution     *compute_distribution_;
  ArrayLayout      *io_layout_;
  Distribution     *io_distribution_;

 
  int              group_io_count_;
  int              read_io_count_;
  int              checkpoint_count_;
  int              op_type_;


  Boolean          interleaved_;     
  Boolean          simulate_;
  Boolean          verify_;

  void             do_init();
  void             delete_arrays();
  void             assign_id();
  void             pack_arrays(int**, Boolean);

 public:

                   ArrayGroup();
                   ArrayGroup(char *);
  virtual         ~ArrayGroup();
  void             insert(Array*);
  void             pack(int**, int*);
  void             unpack(int**);
  void             timestep();
  void             general_write();
  void             checkpoint();
  void             restart();
  void             read_timestep();
  void             general_read();
  Array           *find_array(int);
  int              op_type();
  void             set_simulate();
  void             reset_simulate();
  void             set_simulate_mode();
  void             reset_simulate_mode();
  Boolean          simulate();
  Boolean          verify();
  void             set_verify();
  void             reset_verify();
  void             set_verify_mode();
  void             reset_verify_mode();
  void             unpack_arrays(int**);
  void             init_array_info(int*,int**);
  void             set_io_strategy(int);
  int              io_strategy();
  int              num_of_arrays();
  void		   clear();
};

#endif

