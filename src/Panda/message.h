#ifndef message_dot_h
#define message_dot_h

    
inline void send_message(void *buf, int count, MPI_Datatype data_type,
		int dest, int tag, MPI_Comm comm)
{
   MPI_Send(buf,count,data_type,dest,tag,comm);
#ifdef DEBUG
   printf("Sending message to %d of size %d with tag %d\n",
		dest, count, tag);
#endif
}

inline void nb_send_message(void *buf, int count, MPI_Datatype data_type,
		int dest, int tag, MPI_Comm comm, MPI_Request *request)
{
   MPI_Isend(buf,count,data_type,dest,tag,comm, request);
#ifdef DEBUG
   printf("Sending nonblocking message to %d of size %d with tag %d\n",
		 dest, count, tag);
#endif
}


inline void receive_message(void *buf, int count, MPI_Datatype datatype,
	int src, int tag, MPI_Comm comm, MPI_Status *status)
{
   MPI_Recv(buf,count,datatype, src,tag,comm,status);
#ifdef DEBUG
   printf("Received message from %d of size %d with tag %d\n",
		src, count, tag);
#endif
}


inline void nb_receive_message(void *buf, int count, MPI_Datatype datatype,
	int src, int tag, MPI_Comm comm, MPI_Request *request)
{
   MPI_Irecv(buf,count,datatype, src,tag,comm,request);
#ifdef DEBUG
   printf("Post a non-blocking receive for %d of size %d with tag %d\n",
		src, count, tag);
#endif
}


inline void mpi_test(MPI_Request *request, int *flag, MPI_Status *status)
{
  MPI_Test(request, flag, status);
}


inline void mpi_get_count(MPI_Status *status, MPI_Datatype datatype, int *len)
{
  MPI_Get_count(status, datatype, len);
}


inline void any_new_message(int *msg_code, int *msg_src,
			int *msg_tag,MPI_Status *msg_status)
{
  int flag;

  MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, msg_status);
  if (!flag){
    *msg_code = NO_MESSAGE;
    *msg_src = -1;
    *msg_tag = -1;
    return;
  }
  else{
    /* There some message waiting for us */
    *msg_tag = msg_status->MPI_TAG;
    *msg_src = msg_status->MPI_SOURCE;
    *msg_code = msg_status->MPI_TAG % 10;
    return;
  }
}

#endif
