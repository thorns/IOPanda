#ifndef Collective_IO_dot_h
#define Collective_IO_dot_h

#include "List.h"
class Array;

class Collective_IO : public Linkable{
 public:
                  Collective_IO();
  virtual        ~Collective_IO();
  virtual Boolean continue_io();
  virtual void     start_to_finish(Boolean, Array*);
  virtual void     compute_node_io_loop(Array*);
};

#endif

  
