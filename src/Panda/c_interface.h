#ifndef _included_C_Interface_h
#define _included_C_Interface_h

#include "CactusExternal/FlexIO/src/IEEEIO.h" 


typedef enum { NONE,
               BLOCK,
               GENERAL,
               CYCLIC
               } Distribution;
typedef struct ArrayInfo {
  char*		name_; 		/* array name */		
  int           rank_;          /* rank */
  int*          size_;          /* glbal size of the array */
  int           esize_;        /* size of each element */
  int           mem_rank_;      /* compute processor topology - rank */
  int*          mem_layout_;    /* compute processor topology - mesh */
  Distribution* mem_dist_;      /* compute processor topology - dist */
  int           disk_rank_;     /* io processor topology - rank */
  int*          disk_layout_;   /* io processor topology - mesh */
  Distribution* disk_dist_;     /* io processor topology - dist */
  char*         data_;          /* data pointer belonging to me */
  int           stencil_width_; /* stencil width */
  struct ArrayInfo *next_;      /* next element */
} ArrayInfo;

/* prototypes for functions called in the Panda library */
int Panda_Create (int ioproc_every, int is_part_time_mode);
void Panda_Finalize (void);
void PandaTimestep (ArrayInfo *);
int PandaIsNewFile (char *);
void Panda_WriteAttribute (const char *, const char *, int, int, const void *);

#endif
