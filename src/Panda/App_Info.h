#ifndef App_Info_dot_h
#define App_Info_dot_h

#include "mpi.h"

class App_Info {
  int              app_num_;
  int              app_size_;
  int              *world_ranks_;
  int              *relative_ranks_;
  MPI_Comm         *intra_comm_;
  int              combine_count_;

 public:
  App_Info(int,int,int*);
  virtual ~App_Info();
  int app_num();
  int app_size();
  int get_master();
  int world_rank(int);
  int relative_rank(int);
  void set_intra_comm(MPI_Comm *);
  MPI_Comm* intra_comm();
  void inc_combine_count();
  int  combine_count();
  void reset_combine_count();
  int *world_ranks();
  void world_ranks(int*);
};

#endif  
