#ifndef definitions_dot_h
#define definitions_dot_h

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include "cctk.h"

extern "C"  {int fsync(int f);}



#define START  0
#define WAITING 1

/* Different I/O strategies */
#define SIMPLE_IO 1
#define CSDIO_IO 2

/* The different possible nodetypes */
#define COMPUTE_NODE 0
#define IO_NODE 1
#define PART_TIME_COMPUTE 2
#define PART_TIME_IO 3
#define SUB_CHUNK 4
#define PART_TIME 5


/* Unix or MPI based file system */
#define MPI_SYSTEM 0
#define UNIX_SYSTEM 1

/* Different kinds of collective I/O operations */
#define RESTART 0
#define READ_TIMESTEP 1
#define GENERAL_READ 2
#define CHECKPOINT 3
#define TIMESTEP 4
#define GENERAL_WRITE 5


/* Tags to indicate the type of the message */

/* #define NO_MESSAGE 10
   #define SPECIAL     9
   #define ARRAYGROUP_SCHEMA 8
   #define CHUNK_DATA_TO_IO  7
   #define APP_IO_DONE 6
   #define QUIT 5
   #define COMP_QUIT 4
   #define CHUNK_SCHEMA 3
   #define CHUNK_DATA_FROM_IO 2
   #define CHUNK_SCHEMA_DATA 1
*/
/* Modified it to make it compatible with my thesis */
#define CHUNK_SCHEMA       1
#define CHUNK_DATA_FROM_IO 2
#define CHUNK_DATA_TO_IO   3

#define COMP_QUIT          4
#define QUIT               5
#define ATTRIBUTE_SCHEMA   6 
#define ATTRIBUTE_DATA     7 

#define ARRAYGROUP_SCHEMA  8
#define SPECIAL            9
#define NO_MESSAGE        10

/* Tags to indicate the type of special operatiosn required */
#define APP_INFO 1
#define APP_BARRIER 2
#define GLOBAL_BARRIER 3
#define CLEANFILES 4
#define FLUSHFILES 5
#define CREATEFILES 6

typedef enum { UNSET,
               Regular,
               Irregular
             } Distribution_Type;

typedef enum { NONE,
	       BLOCK,
	       GENERAL,
               CYCLIC
	       } Distribution;

typedef enum { HPF,
               NAS,
               GENERAL_BLOCK
               } Block_Distribution;

typedef enum { ROUND_ROBIN,
               REGULAR
               } ChunkAllocPolicy;


typedef enum { NO = 0,
               YES = 1
	       } Boolean;


typedef enum { ALLOC,
	       NO_ALLOC,
	       SHARED
	       } DataStatus;
		 


inline int max(int a, int b)
{
  if (a > b) return a;
  else return b;
}

inline int min(int a, int b)
{
  if (a < b) return a;
  else return b;
}


inline int* copy_int_list(int s, int *l)
{
  int *ret_list = (int *) malloc(sizeof(int)*s);
  for(int i=0;i<s;i++)
    ret_list[i] = l[i];
  return ret_list;
}



inline Distribution* copy_distribution(int num, Distribution *ptr)
{
  Distribution *ret_list = (Distribution *)malloc(sizeof(Distribution)*num);
  
  for(int i=0; i < num; i++)
     ret_list[i] = ptr[i];

  return ret_list;
}


inline Boolean equal_distribution(int size, Distribution* dist1, Distribution* dist2)
{
  for(int i=0; i < size; i++)
  {
    if (dist1[i] != dist2[i])
	return NO;
  }
  return YES;
}

inline void pack_distribution(int **schema_buf, int rank, Distribution *in_dist)
{
  Distribution *dist = in_dist;
  int* ptr = *schema_buf;
  
  for(int i=0;i<rank;i++)
  	*ptr++ = (int) dist[i];  
  *schema_buf = ptr;
}

inline Distribution* new_distribution(int **schema_buf, int rank)
{
  Distribution *dist = (Distribution*) malloc(sizeof(Distribution)*rank);
  int *ptr = *schema_buf;
  
  for(int i=0;i<rank;i++)
     dist[i] = (Distribution) *ptr++;

  *schema_buf = ptr;
  return  dist;

}

inline int num_elements(int r, int *size)
{
  int total=1;
  for(int i=0;i<r;i++) total *= size[i];
  return total;
}

           
#endif
