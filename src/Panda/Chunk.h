#ifndef Chunk_dot_h
#define Chunk_dot_h

#include "mpi.h"
#include "List.h"
#include "ArrayLayout.h"

class Array;


class Chunk : public Template, public Linkable {
 protected:
  int              *base_;
  int              *stride_;
  int              chunk_id_;  /* This should be unique */
  int              element_size_;
  Array*           array_;
  Chunk*           chunk_;
  char             *data_ptr_;
  int		   stencil_width_;
  DataStatus       data_status_;
  Boolean          am_subchunk_;


  void             compute_first_last_chunk(int, int*, 
					    ArrayLayout*,Distribution*,
					    Block_Distribution, int*, 
					    int*);
  void             do_init(Array*,int,int, DataStatus);
  void             do_init(Chunk*,int,DataStatus);
  void             clear();

 public:
                   Chunk();
                   Chunk(Array*,int*,int*);
                   Chunk(Array*,int,int,DataStatus);
                   Chunk(Chunk*, int, DataStatus);
  void             init(Array*,int,int,DataStatus);
  void             init(Chunk*,int,DataStatus);
  virtual         ~Chunk();
  void             chunk_overlaps(Array *, int*, int*, int);
  int              total_size_in_bytes();
  int              total_size_in_elements();
  int              chunk_id();
  void            *data_ptr();
  void             set_data_ptr(char *);
  void             set_stencil_width(int);
  int*             base();
  int*             stride();
  int*             size();
  int              element_size();
  void             base_offset(int*, void**);
  void             compute_overlap(Chunk*,int*,int*,int*);
  void             convert_from_number_to_index(int,int*);
  void             calculate_base_size_stride(int, int*, int*, int*,
					ArrayLayout*, Distribution*,
					Block_Distribution, int);
  Array*           array();
  Boolean          am_subchunk();
  void             copy_base_size_stride(int*,int*, int*);
  void             make_datatype(int*,int*,int*,void**,MPI_Datatype*);      

};

#endif

 

