#ifndef Panda_dot_h
#define Panda_dot_h

#include "VirtFS.h"
#include "MPIFS.h"


class Panda {
  int file_system_type_;
  VirtFS *file_system_;

public:
  Panda();
  Panda(int, int, int,int, int*);
  Panda(int, int, int,int, int*, Boolean);
  Panda(int,int,int,int*,int,int,int*);
  Panda(int);
  Panda(int, int);
  ~Panda();

  /* stuff required only for testing purposes */
  void global_barrier();
  void app_barrier();
  void cleanfiles();
  void flushfiles();
  void createfiles();
};

#endif

  
