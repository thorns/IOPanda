#ifndef ArrayLayout_dot_h
#define ArrayLayout_dot_h

#include "Template.h"

class ArrayLayout : public Template {
 /* Inherits rank_,size_ from Template */
 public:
                ArrayLayout(int Rank, int *sizearray);
                ArrayLayout(int** schema_buf);        
                ArrayLayout(ArrayLayout *old_layout); 
     virtual    ~ArrayLayout();
     void       pack(int** schema_buf);          
     int        convert_from_index_to_number(int *indices);
     int*       convert_from_number_to_index(int num);
     void       convert_from_number_to_index(int num, int *result);
     Boolean    valid_index(int *);             
     Boolean    valid_distribution(int, Distribution*);
     Boolean    equal(ArrayLayout*);
     int        size(int);
     int*       size();
     void       indices_list(int*, int*, int*, int*);
     void       calculate_indices(int*,int*,int,int,int**);
};

#endif
