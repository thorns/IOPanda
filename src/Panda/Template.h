#ifndef template_dot_h
#define template_dot_h

class Template {
 protected:
  int             rank_;
  int             *size_;

 public:
                  Template();
                  Template(int Rank, int *sizearray);
  virtual        ~Template();
  int             rank();  
  int*            size();
  int             size(int);
  int             total_elements();
};

#endif

