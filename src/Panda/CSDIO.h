#ifndef CSDIO_dot_h
#define CSDIO_dot_h

#include "Simple_IO.h"
class ArrayGroup;
class Array;
class App_Info;
class Chunk;

class CSDIO : public Simple_IO
{
 protected:
  int               compute_pending_;
  int               max_pending_;
  MPI_Datatype      *comp_datatypes_;
  MPI_Request       *comp_requests_;
  MPI_Status        *comp_statuses_;
  int               comp_array_rank_;
  int               max_comp_rank_;
  int               *comp_overlap_base_;
  int               *comp_overlap_size_;
  int               *comp_overlap_stride_;
  int               io_max_overlaps_;
  int               io_overlaps_;
  int               *io_overlap_chunk_ids_;
  int               *io_dest_ids_;
  Array             *comp_current_array_;
  int               comp_current_array_id_;
  int               comp_current_chunk_id_;
  int               comp_current_subchunk_id_;
  int               comp_num_of_subchunks_;
  App_Info          *io_app_info_;
  
  Boolean         process_compute_side_array(ArrayGroup*,int,Boolean);
  void            clear();
  void            do_init();
  void            receive_io_app_info();
  virtual void    send_schema_message(int,int);
  virtual void    send_data_to_compute_nodes(Chunk*,int);
  virtual void    receive_data_from_compute_nodes(Chunk*,int);
  void            realloc_compute_schema_bufs(int);
  void            realloc_pending_messages(int);
  void            realloc_io_buffers(int);
  void            io_chunk_overlaps(Array*,Chunk*);
  void            wait_for_completion();

  
 public:
                  CSDIO(int*,int,int,int,int,App_Info*);
                  CSDIO(int*,int,int,int,int,App_Info*,Boolean);
                  CSDIO();
  virtual         ~CSDIO();
  virtual void    start_to_finish(Boolean, ArrayGroup*);
  virtual void    compute_node_io_loop(ArrayGroup*);  
  virtual char*   name();
};


  
#endif
