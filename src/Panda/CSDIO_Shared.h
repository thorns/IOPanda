#ifndef CSDIO_Shared_dot_h
#define CSDIO_Shared_dot_h

#include "CSDIO.h"
class Chunk;

class CSDIO_Shared : public CSDIO
{
 protected:
  int             current_array_id_;
  int             status_flag_;
  Chunk           *subchunk_;
  Chunk           *compute_chunk_;
  Boolean         read_op_;
  int             bytes_to_go_;
  int             make_subchunks_;
  int             tag_;
  
  Boolean         get_next_chunk();
  Boolean         get_next_array();
  Boolean         get_next_subchunk();
  Boolean         start_next_subchunk_io();
  void            start_subchunk_io();
  Boolean         test_subchunk_io();
    
 public:
                  CSDIO_Shared(int*,int,int,int,int, App_Info*);
  virtual        ~CSDIO_Shared();
  virtual Boolean continue_io();
};

#endif
  
