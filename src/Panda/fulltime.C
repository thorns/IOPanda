/*****************************************************************
 *     This is a sample program that shows how the panda library *
 *     is going to be used by the application programs.          *
 *     The example command line format is in test7.script.       *
 *     This example shows the interface with only disk layout    *
 *     info but no stride or subchunking schema. The value for   *
 *     those schemas use the default ones.                       *
 *     The current test varies the size of arrays. However, the  *
 *     wrapper function allows the number of the nodes to be     *
 *     changed as well.                                          *
 *     The first iteration loads all the code in memory.         *
 *     The second run does the simulated disk simulation.        *
 *     From the third run on, the values are the real writes.    *
 *****************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "definitions.h"
#include "StopWatch.h"
#include "ArrayGroup.h"
#include "ArrayLayout.h"
#include "Array.h"
#include "Panda.h"

int Num_of_Arrays = 1;
int Num_Simulate_Read = 0;
int Num_Read = 0;
int Num_Simulate_Write = 2;
int Num_Write = 2 ;
int interleave = 0;
Panda *global_bear;
extern int SUBCHUNK_SIZE;
int STRATEGY = 1;
int BLK;

int CYCLIC_ON_MEM = 0;

void test_timestep(ArrayGroup *t1, int arraysize, Array **arrays)
{
  StopWatch timer;
  int       i;
  int       flag=0;
  char      time_message[100];

#ifdef VERIFYBF
    for (int j=0; j<Num_of_Arrays; j++) arrays[j]->set_byte_pattern();
    t1->set_verify();
#endif


  global_bear->cleanfiles();
  global_bear->createfiles();


  for (i=0; i<Num_Simulate_Write+Num_Write; ++i) {
     if (i < Num_Simulate_Write){
              t1->set_simulate_mode();
              flag=0;
	    }
     else {
              t1->reset_simulate_mode();
              flag=1;
	   }


    global_bear->app_barrier();
    t1->set_io_strategy(STRATEGY);
    timer.start();
    t1->timestep();
    timer.stop(":");
    sprintf(time_message,"%s Write: SIZE: %d, BLK: %d, Time %i %s",
		(flag==0? "Simulated":"Real"), 
		arraysize, BLK, i, timer.get_description());
    printf("%s", time_message);

    if (Num_Read + Num_Simulate_Read == 0 || i < Num_Simulate_Write + Num_Write-1  ) {
	  global_bear->cleanfiles();
	  global_bear->createfiles();

    }  
   }
}

void test_readtimestep(ArrayGroup *r1, int arraysize, Array **arrays)
{
  StopWatch timer;
  int       i;
  int       flag;
  char time_message[100];
#ifdef VERIFYBF
      for (int j=0; j<Num_of_Arrays; j++) arrays[j]->reset_byte_pattern(); 
#endif

  if (Num_Write + Num_Simulate_Write == 0) {
	  global_bear->cleanfiles();
	  global_bear->createfiles();
  }  



  for (i=0; i<Num_Simulate_Read+Num_Read; ++i) {
    if (i < Num_Simulate_Read) { r1->set_simulate_mode(); flag=0; }
    else {r1->reset_simulate_mode();
          flag=1;
	  global_bear->flushfiles();
     }


    global_bear->app_barrier();
    r1->set_io_strategy(STRATEGY);
    timer.start();
    r1->read_timestep();
    timer.stop(":");

    sprintf(time_message,"%s Read: SIZE: %d, Time %i %s ", 
	(flag==0? "Simulated":"Real"),
	arraysize, i, timer.get_description());
    printf("%s", time_message);
 }
#ifdef VERIFYBF
    for(i=0;i<Num_of_Arrays;i++)
	if (arrays[i]->verify_byte_pattern())
		printf("Byte pattern verified for array %d\n", i);
	else
		printf("Byte pattern incorrect for array %d\n", i);
#endif
    global_bear->cleanfiles();
}
 
 
int gemein(Panda *bear, int io_nodes, int arrayrank, int *arraysize, int esize,
           int mrank, int *mlayout, int drank, int *dlayout,
           Distribution *mem_dist, int* blk_size, Distribution *disk_dist, int cost_model)
{
  ArrayLayout *mem1;              // Memory array layout
  ArrayLayout *disk1;             // Disk array layout
  int i;
  Array **arrays;
  arrays = (Array **)malloc(sizeof(Array*)*Num_of_Arrays);

// Set up memory and disk layouts
  mem1 = new ArrayLayout (mrank,mlayout);
  disk1 = new ArrayLayout(drank,dlayout);

// Create an Array for computation. 
  char *name;
  name = (char *)malloc(sizeof(char)*(strlen("z1Array")+5));
  char temp[5];
  for (i=0; i< Num_of_Arrays; i++) {
    strcpy(name,"z1Array");
    sprintf(temp, "%d", i);
    strcat(name, temp);
    arrays[i] = new Array(name,arrayrank,arraysize,esize,
		     mem1,mem_dist,disk1, disk_dist);
  }
  free(name);

  if (Num_Simulate_Write + Num_Write > 0) {
    ArrayGroup *t1 = new ArrayGroup("z4timestep");
    for (i= 0; i<Num_of_Arrays; i++)  t1->insert(arrays[i]);
    test_timestep(t1, arraysize[arrayrank-1], arrays);
    delete t1;
    if (Num_Simulate_Read + Num_Read > 0) {
      ArrayGroup *r1 = new ArrayGroup("z4timestep");
      for (i= 0; i<Num_of_Arrays; i++)  r1->insert(arrays[i]);
      test_readtimestep(r1, arraysize[arrayrank-1], arrays);
      delete r1;
     }  
  } else {

    ArrayGroup *r1 = new ArrayGroup("z4timestep");
    for (i= 0; i<Num_of_Arrays; i++)  r1->insert(arrays[i]);
    test_readtimestep(r1, arraysize[arrayrank-1], arrays);
    delete r1;
  }

  // delete all objects created

  for (i=0; i<Num_of_Arrays; i++) delete arrays[i];
  free(arrays);
  delete disk1;
  delete mem1;
  return(0);
}

char my_getopt(char *str)
{
  char command[24][15];

  strcpy(command[0], "-Total_nodes");
  strcpy(command[1], "-Io_nodes");
  strcpy(command[2], "-upper");
  strcpy(command[3], "-Arraysize");
  strcpy(command[4], "-Esize");
  strcpy(command[5], "-Mlayout");
  strcpy(command[6], "-Dlayout");
  strcpy(command[7], "-mem_dist");
  strcpy(command[8], "-disk_dist");
  strcpy(command[9], "-num_arrays");
  strcpy(command[10], "-read_simulate");
  strcpy(command[11], "-Read");
  strcpy(command[12], "-write_simulate");
  strcpy(command[13], "-Write");
  strcpy(command[14], "-interleave");
  strcpy(command[15], "-Cost_model");
  strcpy(command[16], "-size_message");
  strcpy(command[17], "-Xfactor");
  strcpy(command[18], "-K");
  
  for (int i= 0; i< 24; i++)  
    if (!strncmp(str, command[i], 2)) return command[i][1];
  printf("undefined input %s, quit!\n",str);
  exit(0); 
}

void parse_cl(int argc, char **argv, int &total_nodes, int &io_nodes, 
	      int &upper_bound, int &lower_bound, int &arrayrank, int*& arraysize,
	      int &esize, int &mrank, int*& mlayout, int& drank, int*& dlayout,
	      Distribution*& mem_dist, int*& blk_size, 
	      Distribution*& disk_dist, int &cost_model_mode, int &upper_blk) 
{
  char opt;
  int k;
 
  for (int i=1; i<argc; ) {
    opt = my_getopt(argv[i++]);
    switch(opt) {
      case 'X':
        STRATEGY = atoi(argv[i++]);
	break;
      case 'T':
        total_nodes =  atoi(argv[i++]); 
	break;
      case 'I': 
	io_nodes = atoi(argv[i++]);
	break;
      case 'u': 
	upper_bound = atoi(argv[i++]);  
	break;
      case 'A': 
	arrayrank = atoi(argv[i++]);
        arraysize = (int *) malloc(sizeof(int)* arrayrank);
        mem_dist = (Distribution *)malloc(sizeof(Distribution)*arrayrank);
	blk_size = (int *) malloc(sizeof(int)*arrayrank);
        disk_dist = (Distribution *)malloc(sizeof(Distribution)*arrayrank);
        for (k = 0; k < arrayrank; k++) arraysize[k] = atoi(argv[i++]);
	lower_bound = arraysize[k-1];
	break;	
      case 'E': 
	esize = atoi(argv[i++]); 
	break;
      case 'M': 
	mrank = atoi(argv[i++]);
	mlayout = (int *) malloc(sizeof(int)* mrank);
        for (k = 0; k < mrank; k++) mlayout[k] = atoi(argv[i++]);
	break;	  
      case 'D':
	drank = atoi(argv[i++]);
	dlayout = (int *) malloc(sizeof(int)* drank);
        for (k = 0; k < drank; k++) dlayout[k] = atoi(argv[i++]);
	break;	
      case 'm':
        for (k = 0; k < arrayrank; k++)
        {
            mem_dist[k] = (Distribution)atoi(argv[i++]);
        }
        break;
      case 'd':
        for (k = 0; k < arrayrank; k++)
        {
            disk_dist[k] = (Distribution)atoi(argv[i++]);
        }
        break;

      case 'n':  
	Num_of_Arrays = atoi(argv[i++]);
        break;
      case 'r': 
	Num_Simulate_Read = atoi(argv[i++]);
        break;
      case 'R':
 	Num_Read = atoi(argv[i++]);
	break;
      case 'w':
	Num_Simulate_Write = atoi(argv[i++]);
	break;
      case 'W':    	
	Num_Write = atoi(argv[i++]);
	break;
      case 'i':
	interleave = atoi(argv[i++]);
	break;
      case 'C':
	cost_model_mode = atoi(argv[i++]);
	break;
      case 's':
	SUBCHUNK_SIZE = atoi(argv[i++]);
	break;

      case 'K':
	upper_blk = atoi(argv[i++]);
	break;

      /* For Panda internal library stuff */


      }
   }

   printf("####### io nodes=%d \n", io_nodes);
} 
   		
int main(int argc, char **argv)
{
  int total_nodes;                // The number of total nodes (comp + io)
  int io_nodes;                   // The number of io nodes
  int upper_bound;                // The upper bound of the last dimension of the array
  int lower_bound;                // The starting number of the last dimension of the array
  int arrayrank ;                 // The array rank.
  int *arraysize;                 // The number of elements along each array dimention
  int esize ;                     // element size of each array element
  int mrank ;                     // Compute node mesh rank
  int *mlayout;                   // Compute node mesh layout
  int drank ;                     // IO node mesh rank
  int cost_model_mode;           // Whether the cost model is included.
  int *dlayout;                   // IO node mesh layout
  Distribution *mem_dist;         // The memory array distribution along each dimention
                                  // There are three possible distributions (BLOCK,
                                  // NONE, CYCLIC).
  int *blk_size;
  Distribution *disk_dist;        // The disk array distribution along each dimention
  int my_rank, my_app_size, *world_ranks, leader;

  int upper_blk;		  // upper bound of the block size
  int lower_blk;
  Panda *bear;

  
  MPI_Init(&argc, &argv);

// For Parallel architecture (IBM SP2 like),
// Initialize the MPI environment. Only compute nodes will return from 
// this call, the io nodes will not return from the call. All the io nodes
  
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &my_app_size);
  leader = 0; 
  world_ranks = (int *) malloc(sizeof(int)*my_app_size);

  parse_cl(argc, argv, total_nodes, io_nodes, upper_bound, lower_bound, 
	arrayrank, arraysize, esize, mrank, mlayout, drank, dlayout, mem_dist, 
	blk_size, disk_dist, cost_model_mode, upper_blk);

  int q = total_nodes/io_nodes;

  for (int i=0; i<io_nodes; i++)
	world_ranks[i] = i*q;
  for (int j=0; j<io_nodes; j++)
      for (int k=1; k< q; k++)
           world_ranks[i++] = j*q + k;

/*
  world_ranks[0] = 0;
  world_ranks[1] = 3;
  world_ranks[2] = 1;
  world_ranks[3] = 2;
  world_ranks[4] = 4;
  world_ranks[5] = 5;

  printf("myrank= %d, io_nodes=%d, total_nodes=%d \n", 
	my_rank, io_nodes, total_nodes);
*/
  printf("world ranks \n");
  for (i=0; i<my_app_size; i++)
   printf(" %d", world_ranks[i]);
  printf("\n\n");
  
  if (my_rank % q == 0)
  { // io nodes
     bear = new Panda(IO_NODE, 0, my_rank/q, io_nodes, world_ranks);
     global_bear = bear;
  }
  else 
  { // compute nodes
     bear = new Panda(COMPUTE_NODE, 1, 
			my_rank/q*(q-1)+(my_rank-1)%q, 
			my_app_size-io_nodes, world_ranks+io_nodes);
     global_bear = bear;

     for (int size=lower_bound; size <= upper_bound; size*=2) 
     {
       arraysize[arrayrank-1] = size; 
       gemein(bear,io_nodes, arrayrank, arraysize, esize, 
              mrank, mlayout, drank, dlayout, mem_dist, blk_size, 
	      disk_dist, cost_model_mode);
     }

  }

  free(arraysize); free(mlayout); free(dlayout); free(mem_dist);
  free(blk_size);
  free(disk_dist);
  free(world_ranks);
  delete bear;

  MPI_Finalize();
  return(0);
}
