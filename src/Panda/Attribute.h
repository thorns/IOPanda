#ifndef Attribute_dot_h
#define Attribute_dot_h

#include "definitions.h"


typedef union int_to_char {
  int i;
  char c[4];
} int_to_char;

//#include "../IEEEIO/IEEEIO.h"
//#include "../IEEEIO/IOProtos.h"
#include "CactusExternal/FlexIO/src/Arch.h"

class Attribute {
  char 	*name_;
  int 	esize_;
  int	count_;
  void	*data_;
  int	data_status_; // 0: no alloc, 1: alloc

public:
	Attribute();
	Attribute(char *, int);
 	~Attribute(); 
  void 	init(char *, int, int, void *);
  void 	init(char *);
  void 	pack(int &, char *&, char *, int);
  void 	write(char *, char *, int, int, void *);
  void 	read(char *, char *);
  void	write_data(IOFile);
  void	read_data(IOFile);
  void	*get_data_ptr();
  void	set_data_ptr(void *);
  int 	data_size();
  int   esize();
  int   count();
  void  set_esize(int);
  void  set_count(int);
};

#endif
