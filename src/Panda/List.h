#ifndef Link_dot_h
#define Link_dot_h


/* This is the dummy base class for all items      *
 * to be placed in a linked list. It would have    *
 * been cleaner to use Templates but support for   *
 * templates varies with different compilers and   *
 * the problem of code blow up etc exists.         *
 */
class Linkable 
{
 public:
                  Linkable(){};
   virtual       ~Linkable(){};
   
};
 

/* The Cells contains a Linkable element and ptrs  *
 * to the next and previos cells                   *
 */
class Cell {
  Linkable         *item_;
  Cell             *next_;
  Cell             *prev_;
 public:
                   Cell();
                   Cell(Linkable*);
                   Cell(Linkable*,Cell*);
                   Cell(Linkable*,Cell*,Cell*);
                  ~Cell();   
  Linkable*        item();
  Cell*            next();
  Cell*            prev();
  void             set_next(Cell*);
  void             set_prev(Cell*);
};


/* The List class provides support for creating a  *
 * list and provides operations like inserting,    *
 * deleting elements to the beginning and the end  *
 * of the list                                     *
 */
class List {
 public:
  Cell             *head_;
  Cell             *tail_;
  Cell             *old_search_val_;   /* result of the previous search */
                   List();
                  ~List();
  void             insert(Linkable*);
  void             add_last(Linkable*);
  void             add_first(Linkable*);
  void             remove(Linkable*);
};

#endif


