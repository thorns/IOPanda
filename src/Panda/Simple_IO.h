#ifndef Simple_IO_dot_h
#define Simple_IO_dot_h

#include "Collective_IO.h"


class ArrayGroup;
class Array;
class Chunk;
class App_Info;

//#include "../IEEEIO/IEEEIO.h"
//#include "../IEEEIO/IOProtos.h"

class Simple_IO : public Collective_IO
{
 protected:
  Boolean       dummy_;  /* Do the instance variables mean anything */
  int           *schema_string_;
  int           schema_size_;
  int           *current_schema_ptr_;
  Array         *current_array_;
  Chunk         *current_chunk_;
  int           num_of_subchunks_;
  int           current_subchunk_id_;
  int           num_of_chunks_;
  int           current_chunk_id_;
  IOFile	file_ptr_;
  FILE          *schema_file_ptr_;
  int           num_io_nodes_;
  int           my_io_rank_;
  int           compute_app_num_;
  App_Info      *app_info_;
  Boolean       part_time_io_;
  Array         *compute_node_array_;
  int		op_type_;
  Boolean       nat_chunked_;
  Boolean       sub_chunked_;
  Boolean	overlaped_;
  Boolean       contiguous_;
  int           world_rank_;

  int           num_overlaps_;
  int           max_overlaps_;
  int           *overlap_chunk_ids_;
  int           *dest_ids_;
  int           **schema_bufs_;
  MPI_Request   *schema_requests_;
  MPI_Request   *requests_;
  MPI_Status    *statuses_;
  MPI_Datatype  *datatypes_;
  int           max_rank_;
  int           array_rank_;
  int           *overlap_base_;
  int           *overlap_stride_;
  int           *overlap_size_;
  char          **data_ptrs_;
  char          *mem_buf_;
  int            mem_buf_size_;  


  void          realloc_buffers(int);
  void          compute_chunk_overlaps(Array*,Chunk*);
  void          compute_schemas(Array*,Chunk*,Chunk*);
  virtual void  send_schema_message(int);
  void          make_datatype(Chunk*,int);
  void          receive_data(Chunk*,int, int&);
  void          send_data(Chunk*, int, int&);
  void          read_data(Chunk*);
  void          read_data(char*,int,int);
  void          write_data(char*,int,int);
  void          write_data(Chunk*);
  void          copy_data(Chunk*,int,Boolean,int&);
  void          direct_io(int,Boolean,int&);
  void          free_datatypes();
  void          wait_for_completion(int&,Array*);
  void          send_data_to_compute_nodes(Chunk*, int&);
  void          receive_data_from_compute_nodes(Chunk*, int&);
  void          realloc_schema_bufs(int);
  void          realloc_mem_bufs(int);
  void          process_compute_message(int&,Array*);
  void          process_chunk_schema_request(int,int,int&,MPI_Status*,Array*);
 public:
                Simple_IO();
                Simple_IO(int*,int,int,int, int , App_Info*, IOFile);
  virtual      ~Simple_IO();
  virtual void  start_to_finish(Boolean part_time_io,Array*);
  virtual void  compute_node_io_loop(Array*);
};

#endif
