#ifndef Shared_IO_dot_h
#define Shared_IO_dot_h

#include "Simple_IO.h"
class Chunk;

class Shared_IO : public Simple_IO
{
 protected:
  int             current_array_id_;
  int             status_flag_;
  Chunk           *subchunk_;
  Chunk           *compute_chunk_;
  Boolean         read_op_;
  int             bytes_to_go_;
  int             make_subchunks_;
  
  Boolean         get_next_chunk();
  Boolean         get_next_array();
  Boolean         get_next_subchunk();
  Boolean         start_next_subchunk_io();
  void            start_subchunk_io();
  Boolean         test_subchunk_io();
    
 public:
                  Shared_IO(int*,int,int,int,int, App_Info*);
  virtual        ~Shared_IO();
  virtual Boolean continue_io();
};

#endif
  
