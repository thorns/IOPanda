#ifndef StopWatch_dot_h
#define StopWatch_dot_h

#include <stdio.h>
#include <mpi.h>

class StopWatch 
{
 private:
  double start_t,finish_t;
  char description[200];

 public:
  StopWatch () { start_t = finish_t = -1; }
  ~StopWatch() {              };
  void start() { start_t = MPI_Wtime(); }
  void stop (char *desc)
               {
		 finish_t = MPI_Wtime();
                 if (start_t == -1.0)
                   fprintf(stderr, "StopWatch: must start before stop\n");
                 else
                   sprintf(description, "%s elapsed time: %f, (%f, %f)\n"
                                  ,desc
                                  ,finish_t-start_t
				  ,start_t, finish_t);
                start_t = finish_t = -1; 
               }
  char *get_description() { return description;}

};


#endif
