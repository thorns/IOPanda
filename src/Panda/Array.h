#ifndef Array_dot_h
#define Array_dot_h

#include "List.h"
#include "ArrayDistribution.h"
#include "Chunk.h"

#include "CactusExternal/FlexIO/src/Arch.h"

//#include "../IEEEIO/IEEEIO.h"
//#include "../IEEEIO/IOProtos.h"


class Array : public Template, public Linkable {
 protected:
  ArrayDistribution	  *compute_node_layout_;
  ArrayDistribution	  *io_node_layout_;
  ArrayDistribution       *subchunk_layout_;
  int                     element_size_;
  int                     ieee_size_;
  char                    *name_;
  Boolean                 natural_chunked_;
  Boolean                 sub_chunked_;
  Boolean                 overlap_;
  int			  op_type_;
  int              	  io_strategy_;

  void                    do_init(char*, int, int*, int, 
				  ArrayLayout*,     Distribution*, 
				  ArrayLayout*,     Distribution*, 
				  ArrayLayout*,     Distribution*, 
				  ChunkAllocPolicy, ChunkAllocPolicy, 
				  Block_Distribution);
  void                    allocate_chunks(int);
  void                    allocate_chunks(int,int,char**,int);
  ArrayDistribution       *unpack_layout(int **);

 public:
                           Array(char*,int, int*, int, ArrayLayout*, 
				 Distribution*, ArrayLayout*,
				 Distribution*);
                           Array(char*,int, int*, int, ArrayLayout*, 
				 Distribution*, ArrayLayout*,
				 Distribution*, char *);
                           Array(char*,int, int*, int, ArrayLayout*, 
				 Distribution*, ArrayLayout*,
				 Distribution*, ArrayLayout*,
				 Distribution*);
                           Array(char*,int, int*, int, ArrayLayout*, 
				 Distribution*, ArrayLayout*,
				 Distribution*, ArrayLayout*,
				 Distribution*, char *);
			   Array(char*,int, int*, int,
                                 ArrayLayout*, Distribution*,
                                 ArrayLayout*, Distribution*, char *, int);
                           Array(int **);
                           Array();
  virtual                 ~Array();
  void			   init(int,int,int*,int);
  Chunk*                   get_next_chunk();
  int                      which_node(int,int,int);
  void                     delete_chunks();
  void                     pack(int**, int*);
  ArrayDistribution*       layout(int);
  int                      which_node(int,int);
  Chunk*                   find_chunk(int);
  int                      element_size();
  int                      ieee_size();
  Boolean                  nat_chunked();
  Boolean                  sub_chunked();
  void                     make_sub_chunks(Chunk*);
  int                      array_info();
  int                      get_next_index(Chunk*&,int,int,int,int);
  int                      num_of_chunks();
  void                     set_data_ptr(char *);
  char*                    get_data_ptr();
  Boolean                  overlaped();
  void			   read_schema_file(IOFile);

  void			   timestep();
  void			   read_timestep();
  void			   checkpoint();
  void			   restart();
  int 			   op_type();
  int			   io_strategy();
};

#endif
