#ifndef ArrayDistribution_dot_h
#define ArrayDistribution_dot_h

#include "definitions.h"
#include "List.h"
#include "ArrayLayout.h"
#include "Chunk.h"

class Array;
class ArrayDistribution
{
protected:
  int                           num_of_chunks_;
  List                          *chunk_list_;
  Cell                          *current_cell_;
public:
                                ArrayDistribution();
                                ArrayDistribution(int **);
  virtual                       ~ArrayDistribution();
  virtual Boolean               equal(ArrayDistribution *);
  virtual int                   distribution_type();
  virtual void                  pack(int **);
  virtual int                   total_elements();
  virtual ChunkAllocPolicy	alloc_policy();
  virtual int                   get_next_index(Chunk *&,int,int,int,int);
  List                          *chunk_list();
  void                          add_last(Chunk *);
  Chunk                         *get_next_chunk();
  void				list_clear();
};


class RegularDistribution : public ArrayDistribution
{
  ArrayLayout           *layout_;
  int                   rank_;
  Distribution          *dist_;
  Block_Distribution    block_dist_;
  ChunkAllocPolicy	alloc_policy_;
public:
                        RegularDistribution(int **);
                        RegularDistribution(int , ArrayLayout *,
                                            Distribution *, ChunkAllocPolicy,
                                            Block_Distribution, int*);
                        RegularDistribution(int , ArrayLayout *,
                                            Distribution *, ChunkAllocPolicy,
                                            Block_Distribution);
                        ~RegularDistribution();
  Boolean               equal(ArrayDistribution *);
  ArrayLayout           *layout();
  Distribution          *distribution();
  int                   distribution_type();
  void                  pack(int **);
  int                   total_elements();
  ChunkAllocPolicy	alloc_policy();
  int                   get_next_index(Chunk *&,int,int,int,int);
  Block_Distribution    block_dist();
};

class IrregularDistribution : public ArrayDistribution
{
public:
			IrregularDistribution(int, Chunk **);
  int                   distribution_type();
  int                   total_elements();
  int                   get_next_index(Chunk *&,int,int,int,int);
};

#endif

